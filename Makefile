.PHONY: all clean

all: cse_graph.svg math_graph.svg

cse_graph.svg: cse.txt.svg cse.txt.json script.js addscript.py
	./addscript.py cse

cse.txt.svg cse.txt.json: cse.txt parse_deps.py
	./parse_deps.py cse.txt

math_graph.svg: math.txt.svg math.txt.json script.js addscript.py
	./addscript.py math

math.txt.svg math.txt.json: math.txt parse_deps.py
	./parse_deps.py math.txt

clean:
	rm *.svg *.json
