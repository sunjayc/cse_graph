unmarked = 'white';
unmet = 'lightgrey';
done = '#11ff11';
able = '#ffff11';
semiable = '#ffffaa';
function init(evt) {
    if(window.svgDoc == null) {
        svgDoc = evt.target.ownerDocument;
    }
    c = JSON.parse(localStorage.getItem("DATANAME"));
    if(c != null) {
        for(var id in courses) {
            courses[id].done = c[id].done;
        }
    }
    setcolor();
    recalculate();
}
function getshape(id) {
    if(courses[id].type == 0) {
      return svgDoc.getElementById(id).getElementsByTagName('ellipse')[0];
    } else {
      return svgDoc.getElementById(id).getElementsByTagName('polygon')[0];
    }
}
function setcolor() {
    for(var id in courses) {
        if(courses[id].done == 1) {
            var ellipse = getshape(id);
            ellipse.setAttribute('fill', done);
        }
    }
}
function changehover(event, id, on) {
    var ids = [id];
    var first = true;
    while(ids.length > 0) {
        var id = ids.shift();
        var ellipse = getshape(id);
        if(on && (courses[id].done == 0 || first)) {
            ellipse.setAttribute('stroke', '#11ffff');
            ellipse.setAttribute('style', 'filter: url(#glow)');
        } else {
            ellipse.setAttribute('stroke', 'black');
            ellipse.setAttribute('style', '');
        }
        if(courses[id].done != 1) {
            for(var i = 0; i < courses[id].prereqs.length; i++) {
                var pre_id = courses[id].prereqs[i];
                if(courses[pre_id].done == 0)
                    ids.push(courses[id].prereqs[i]);
            }
        }
        first = false;
    }
}
function changecolor(event, id) {
    var ellipse = getshape(id);
    if(courses[id].done == 0) {
        changehover(event, id, false);
        ellipse.setAttribute('fill', done);
        courses[id].done = 1;
        changehover(event, id, true);
    } else if(courses[id].done == 1) {
        ellipse.setAttribute('fill', unmarked);
        courses[id].done = 0;
        changehover(event, id, true);
    }
    localStorage.setItem("DATANAME", JSON.stringify(courses));
    recalculate();
}
function recalculate() {
    var changed = false;
    do {
        changed = recalculate_helper();
    } while(changed);
}
function recalculate_helper() {
    var changed = false;
    for(var id in courses) {
        var course = courses[id];
        course.prereqs_met = false;
        if(course.type != 0 || course.done == 0) {
            var prereqs_met = (course.type == 2 || course.prereqs.length == 0) ? false : true;
            for(var i = 0; i < course.prereqs.length; i++) {
                var pre_id = course.prereqs[i];
                if(courses[pre_id].done != 1 && course.type != 2) {
                    prereqs_met = false;
                    break;
                } else if(course.type == 2 && courses[pre_id].done == 1) {
                    prereqs_met = true;
                    break;
                }
            }
            var shape = getshape(id);
            if(prereqs_met) {
                course.prereqs_met = true;
                if(course.type == 0) {
                    var rec_met = true;
                    for(var i = 0; i < course.recommended.length; i++) {
                        var rec_id = course.recommended[i];
                        if(courses[rec_id].done != 1) {
                            rec_met = false;
                            break;
                        }
                    }
                    if(rec_met)
                        shape.setAttribute('fill', able);
                    else
                        shape.setAttribute('fill', semiable);
                } else if(course.done != 1) {
                    shape.setAttribute('fill', done);
                    course.done = 1;
                    changed = true;
                }
            } else {
                shape.setAttribute('fill', course.type == 0 ? unmarked : unmet);
                if(course.type != 0 && course.done != 0) {
                    course.done = 0;
                    changed = true;
                }
            }
        }
    }
    return changed;
}
