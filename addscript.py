#!/usr/bin/python3

import xml.etree.ElementTree as etree
import json
from sys import argv

ns = {'def': 'http://www.w3.org/2000/svg', 'xlink': 'http://www.w3.org/1999/xlink'}
etree.register_namespace('', ns['def'])
etree.register_namespace('xlink', ns['xlink'])

gcodes = etree.parse(argv[1] + '.txt.svg')
svg = gcodes.getroot() # The svg tag is the root tag

title = svg.find('def:g/def:title', ns)
title.text = '';

fp = open('script.js', 'r')
script = etree.Element('script', {'type': 'text/ecmascript'})
script.text = fp.read().replace('DATANAME', argv[1]);
fp.close()

fp = open(argv[1] + '.txt.json', 'r')
jsondata = json.load(fp)
fp.seek(0)
script.text += 'courses = ' + fp.read()
svg.insert(0, script)
fp.close()

filt = etree.XML("""
<filter id="glow">
    <feColorMatrix type="matrix"     
        values=
            "0 0 0 0   0
             0 0 0 0.9 0 
             0 0 0 0.9 0 
             0 0 0 1   0"/>
    <feGaussianBlur stdDeviation="2.5"     
        result="coloredBlur"/>     
    <feMerge>     
        <feMergeNode in="coloredBlur"/>
        <feMergeNode in="SourceGraphic"/>
    </feMerge>
</filter>
""")
svg.insert(1, filt)

svg.set('onload', 'init(evt)')

nodes = svg.findall("def:g[@class='graph']/def:g[@class='node']", ns)
for node in nodes:
    course = node.findtext('def:title', '', ns)
    if jsondata[course]['type'] == 0:
        id = node.get('id')
        node.set('onclick', "changecolor(arguments[0], '" + id + "')")
        node.set('onmouseover', "changehover(arguments[0], '" + id + "', true)")
        node.set('onmouseout', "changehover(arguments[0], '" + id + "', false)")
        node.set('cursor', 'pointer')

gcodes.write(argv[1] + '_graph.svg')
