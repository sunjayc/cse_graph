#!/usr/bin/python3
import pydot
import json
from sys import argv

LEVELED = True

def makeNode(name, tooltip='', **attrs):
    n = pydot.Node(name, **attrs)
    n.set_style('filled')
    n.set_fillcolor('white')
    n.set_id(name)
    if tooltip is not None:
        if tooltip is '':
            tooltip = name
        n.set_tooltip(tooltip)
    else:
        n.set_tooltip('')
    n.type = 0
    return n

def splitOnChar(z, c):
    if z not in nodes:
        nodes[z] = makeNode(z, None, label='')
        nodes[z].set_width(.25)
        nodes[z].set_height(.25)
        nodes[z].set_fillcolor('')
        if c == 'X':
            nodes[z].set_shape('invtriangle')
            nodes[z].type = 1;
        elif c == 'O':
            nodes[z].set_shape('diamond')
            nodes[z].type = 2;
        elif c == 'x':
            nodes[z].set_shape('invtriangle')
            nodes[z].type = 1;
        elif c == 'o':
            nodes[z].set_shape('diamond')
            nodes[z].type = 2;
    x = z.split(c)
    if z not in links:
        links[z] = []
    for n in x:
        if 'X' in n:
            splitOnChar(n, 'X')
        elif 'O' in n:
            splitOnChar(n, 'O')
        elif 'x' in n:
            splitOnChar(n, 'x')
        elif 'o' in n:
            splitOnChar(n, 'o')
        elif n not in nodes:
            nodes[n] = makeNode(n)
        if n not in links[z]:
            links[z].append(n)

def assignLevel(course):
    if course not in links:
        nodes[course].level = 0
        return 0
    elif not hasattr(nodes[course], 'level'):
        max = 0
        for prereq in links[course]:
            if not hasattr(nodes[prereq], 'level'):
                assignLevel(prereq)
            if nodes[prereq].level > max:
                max = nodes[prereq].level
        max += 1
        if 'x' not in course and 'o' not in course:
            max = (max - (max % 3)) + 2
        nodes[course].level = max
        return max + 1
    else:
        return nodes[course].level + 1

f = open(argv[1])
nodes = {}
links = {}
recc = {}
conc = {}
for x in f:
    x = x.strip()
    if x[0] == '#':
        continue
    if x == '----':
        break
    hasPrereq = False
    if ': ' in x:
        (x, y) = x.split(': ')
        hasPrereq = True
    if '#' in x:
        (x, w) = x.split('#')
    else:
        w = x;
    nodes[x] = makeNode(x, w)
    if not hasPrereq:
        continue
    y = y.split(', ')
    i = 0
    for z in y:
        if z == '':
            i += 1
            continue
        if 'X' in z:
            splitOnChar(z, 'X')
        elif 'O' in z:
            splitOnChar(z, 'O')
        elif 'x' in z:
            splitOnChar(z, 'x')
        elif 'o' in z:
            splitOnChar(z, 'o')
        elif z not in nodes:
            nodes[z] = makeNode(z)
        if i == 0:
            if x not in links:
                links[x] = []
            links[x].append(z)
        elif i == 1:
            if x not in recc:
                recc[x] = []
            recc[x].append(z)
        else:
            if x not in conc:
                conc[x] = []
            conc[x].append(z)
        i += 1

jsondata = {}
for course in nodes:
    jsondata[course] = {'prereqs': [], 'recommended': [], 'concurrent': [], 'done': 0, 'type': nodes[course].type}
    if course in links:
        for prereq in links[course]:
            jsondata[course]['prereqs'].append(prereq)
    if course in recc:
        for rec in recc[course]:
            jsondata[course]['recommended'].append(rec)
    if course in conc:
        for con in conc[course]:
            jsondata[course]['concurrent'].append(con)
fp = open(argv[1] + '.json', 'w')
json.dump(jsondata, fp)
fp.close()

g = pydot.Dot("REMOVE_TITLE")

levels = []
max = 0
for course in nodes:
    level = assignLevel(course)
    if level > max:
        max = level
        
levels = [None] * (max)
if LEVELED:
    for course in nodes:
        level = nodes[course].level
        if not levels[level]:
            levels[level] = pydot.Subgraph(rank = 'same')
        levels[level].add_node(nodes[course])
    lnodes = []
    def getLevel(node):
        if hasattr(node, 'level'):
            return node.level
        return 0
    for sg in levels:
        if sg:
            n = sg.get_nodes()[0]
            lnodes.append(n)
    lnodes.sort(key = getLevel)
    for i in range(len(lnodes) - 1):
        g.add_edge(pydot.Edge(lnodes[i], lnodes[i + 1], style = 'invis'))
    for subgraph in levels:
        if subgraph:
            g.add_subgraph(subgraph)
else:
    for course in nodes:
        level = nodes[course].level
        if not levels[level]:
            levels[level] = []
        levels[level].append(nodes[course])
    for level in levels:
        if level:
            for node in level:
                g.add_node(node)

        
edges = {}
for course in links:
    for prereq in links[course]:
        e = pydot.Edge(prereq, course)
        if 'x' in course or 'o' in course:
            e.set_arrowhead('none')
        edges[(prereq, course)] = e
for course in recc:
    for prereq in recc[course]:
        e = pydot.Edge(prereq, course, style = 'dotted')
        if 'x' in course or 'o' in course:
            e.set_arrowhead('none')
        edges[(prereq, course)] = e
for course in conc:
    for prereq in conc[course]:
        e = pydot.Edge(prereq, course, style = 'dashed')
        if 'x' in course or 'o' in course:
            e.set_arrowhead('none')
        edges[(prereq, course)] = e
        

for link in edges:
    g.add_edge(edges[link])



#g.set_ranksep('1.0')


g.write_svg(argv[1] + '.svg')
g.write_dot(argv[1] + '.dot')
g.write_png(argv[1] + '.png')
